const fetch = require("node-fetch");
const cheerio = require("cheerio");
const CronJob = require("cron").CronJob;
const Telegraf = require("telegraf");
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const token = "1029297458:AAGqHei1hkUsxvkcVvZa7-_sVIIBo4dhEY4";
const app = new Telegraf(token);

const getDetailroduct = url =>
  new Promise((resolve, reject) => {
    fetch(`https://sepatucompass.com${url}`, {
      method: "GET",
      headers: {
        Referer: "https://sepatucompass.com/",
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
      }
    })
      .then(res => res.text())
      .then(async res => {
        const $ = await cheerio.load(res);
        const newProd = [];
        // const prod = await $('button.single_add_to_cart_button.button.alt').text();
        const productName = $('h1.h2.product-single__title').text();
        const productPrice = $('span.product__price').text();
        const prod = await $(
          "button.btn.btn--full.add-to-cart.btn--secondary"
        ).text();
        const dataProduct = {
          productName: productName.replace(/\r\n|\r|\n/g, " ").trim(),
          productPrice: productPrice.replace(/\r\n|\r|\n/g, " ").trim(),
          prodStatus: prod.replace(/\r\n|\r|\n/g, " ").trim()
        }
        resolve(dataProduct);
      })
      .catch(err => reject(err));
  });

const getListProducts = url =>
  new Promise((resolve, reject) => {
    fetch(`https://sepatucompass.com/collections/all`, {
      method: "GET",
      headers: {
        Referer: "https://sepatucompass.com/",
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
      }
    })
      .then(res => res.text())
      .then(async res => {
        const $ = await cheerio.load(res);
        const newProd = [];
        // const prod = await $('button.single_add_to_cart_button.button.alt').text();
        const prod = await $(
          "div.grid.grid--uniform.grid--collection a.grid-product__link.grid-product__link--disabled"
        ).each(function(i, element) {
          newProd.push($(this).attr("href"));
        });
        resolve(newProd);
      })
      .catch(err => reject(err));
  });

(async () => {
  console.log("");
  console.log("=== STARTING SCRIPT ===");
  console.log("");

  app.hears('cekproduk', async (ctx) => {
    const listProducts = await getListProducts();
    app.telegram.sendMessage(484279002, 'Mohon Tunggu Sedang Mengecek.');
    let newData = [];
    for (let index = 0; index < listProducts.length; index++) {
      const prodUrl = listProducts[index];
      const checkStatus = await getDetailroduct(prodUrl);
      newData.push({
        'nama produk': checkStatus.productName,
        'harga produk': checkStatus.productPrice,
        'status produk': checkStatus.prodStatus,
        'produk url': `https://sepatucompass.com${prodUrl}`
      }) 
    }

    app.telegram.sendMessage(
      484279002,
      JSON.stringify(newData, null, 4)
    );
  })

  const job = new CronJob("*/5 * * * *", async function() {
    const d = new Date();
    console.log("Every 5 Minute : ", d);

    const listProducts = await getListProducts();

    

    for (let index = 0; index < listProducts.length; index++) {
      const prodUrl = listProducts[index];
      const checkStatus = await getDetailroduct(prodUrl);
      console.log(
        checkStatus.prodStatus,
        `=> https://sepatucompass.com${prodUrl}`
      );

      if (checkStatus.prodStatus != 'Sold Out') {
        app.telegram.sendMessage(
          484279002,
          `nama product : ${checkStatus.productName}\nharga product : ${checkStatus.productPrice}\nstatus product : ${checkStatus.prodStatus}\nproduct url : https://sepatucompass.com${prodUrl}`
        ); //rubah 285431595 ke chat_id bot yang disediakan
      }
    }
    console.log("");
  });
  job.start();
  app.launch()

  
})();
